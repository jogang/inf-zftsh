#include <algorithm>
#include <iostream>
#include <map>
#include <vector>

using ll = long long;

// выдаёт количество каждого простого делителя числа
std::map<ll, ll> prime_dividers(ll n) {
    std::map<ll, ll> ans;
    while (n > 1) {
        ll c = 2;
        while ((c * c <= n) && (n % c != 0)) {
            c++;
        }
        if (n % c == 0) {
            ans[c]++;
            n /= c;
        } else {
            ans[n]++;
            n = 1;
        }
    }
    return ans;
}

// возведение в степень
ll pow(ll n, ll power) {
    ll ans = 1;
    if (power <= 0) {
        ans = 1;
    } else if (power % 2 == 0) {
        ans = pow(n, power / 2);
        ans *= ans;
    } else {
        ans = pow(n, power - 1) * n;
    }
    return ans;
}

int main() {
    std::vector<ll> v(0);
    int c;
    std::cin >> c;
    while (c != 0) {
        v.push_back(c);
        std::cin >> c;
    }
    std::map<ll, ll> gcd_dividers, lcm_dividers;

    // если первый элемент отдельно не рассмотреть, то в gcd_dividers все
    // значения будут нулями
    auto current_dividers = prime_dividers(v[0]);
    for (auto [divider, count] : current_dividers) {
        gcd_dividers[divider] = count;
        lcm_dividers[divider] = count;
    }

    // на gcd делятся все числа - делителей <= чем в любом числе
    // lcm делится на все числа - делителей >= чем в любом числе
    for (auto x : v) {
        auto current_dividers = prime_dividers(x);
        for (auto [divider, count] : current_dividers) {
            gcd_dividers[divider] = std::min(gcd_dividers[divider], count);
            lcm_dividers[divider] = std::max(lcm_dividers[divider], count);
        }
    }
    ll gcd = 1, lcm = 1;
    for (auto [divider, count] : gcd_dividers) {
        gcd *= pow(divider, count);
    }
    for (auto [divider, count] : lcm_dividers) {
        lcm *= pow(divider, count);
    }

    std::cout << gcd << " " << lcm << "\n";
}