#include <iostream>

std::pair<int, int> f(int x) {
    int a = 0;
    int b = 0;
    while ((x > 0) && x >= 3) {
        if (x % 2 == 0) {
            a += x % 3;
        } else {
            b += x % 5;
        }
        x = x / 3;
    }
    return {a, b};
}

int main() {
    int x = 0;
    while (f(x) != std::make_pair(5, 9)) {
        x++;
    }
    std::cout << x << "\n";
}