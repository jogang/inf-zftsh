#include <iostream>
#include <map>

std::map<int, int> days_in_month = {{1, 31}, {2, 28},  {3, 31},  {4, 30},
                                    {5, 31}, {6, 30},  {7, 31},  {8, 31},
                                    {9, 30}, {10, 31}, {11, 30}, {12, 31}};

std::map<int, std::string> day_mod_to_day_name = {
    {1, "понедельник"}, {2, "вторник"}, {3, "среда"},      {4, "четверг"},
    {5, "пятница"},     {6, "суббота"}, {0, "воскресенье"}};

// проверка високосности года
bool year_is_leap(int year) {
    bool ans;
    if (year % 400 == 0)
        ans = true;
    else if (year % 100 == 0)
        ans = false;
    else if (year % 4 == 0)
        ans = true;
    else
        ans = false;
    return ans;
}

// сколько дней прошло с начала года до начала данного месяца
int month_to_days(int month, int year) {
    int ans = 0;
    for (int i = 1; i < month; ++i) {
        if (i == 2 && year_is_leap(year))
            ans += 29;
        else
            ans += days_in_month[i];
    }
    return ans;
}

// сколько дней прошло с 0 января 1 года до начала данного года
int year_to_days(int year) {
    int ans = 0;
    for (int i = 1; i < year; ++i) {
        ans += year_is_leap(i) ? 366 : 365;
    }
    return ans;
}

int main() {
    int d, m, y;
    std::cin >> d >> m >> y;
    int abs_day = year_to_days(y) + month_to_days(m, y) + d;
    // 1 января 1 года (abs_day == 1) - понедельник
    std::cout << day_mod_to_day_name[abs_day % 7] << "\n";
}