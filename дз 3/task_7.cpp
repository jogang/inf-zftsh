#include <cmath>
#include <iomanip>
#include <iostream>

using ld = long double;
using ll = unsigned long long;

// площадь круга = pi * r^2
// возьмём квадрат размера 2^31 * 2^31
// впишем в него окружность, её радиус будет 2^30, площадь будет pi * 2^60
// площадь квадрата = (2^31)^2 = 2^62
// будет случайно ставить точки в этот квадрат, тогда какие-то точки попадут в
// окружность, а какие-то нет
// количество точек в окружности / полное количество точек = pi / 4
// квадрат от (0, 0) до (2^31, 2^31)
// круг: (x - 2^30)^2 + (y -  2^30)^2 <= (2^30)^2 = 2^60
//        ^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                  <= 2^61

// RAND_MAX = 2^31, long long max = 2^63

const ll POINTS = 1e8;
const ll p2_30 = 1073741824, p2_60 = 1152921504606846976;  // степени двойки

int main() {
    ll points_inside_circle = 0;
    for (ll i = 0; i < POINTS; ++i) {
        ll x = std::rand();  // x, y <= 2^31
        ll y = std::rand();
        points_inside_circle +=
            ((x - p2_30) * (x - p2_30) + (y - p2_30) * (y - p2_30) <= p2_60)
                ? 1
                : 0;
    }
    ld pi = points_inside_circle * 4.0 / POINTS;
    std::cout << std::fixed << std::setprecision(9) << pi << "\n";
}