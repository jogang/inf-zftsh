# как посчитать на c++ число больше 2^63 без boost я не знаю

MAX_NUMBER = int(pow(10, 5))
CRITICAL_NUMBER = int(pow(10, 100))


# решето эратосфена
def create_n_primes(n: int) -> list[int]:
    ans: list[int] = []
    used_numbers: dict[bool] = {}
    x = 2
    while len(ans) < n and x < MAX_NUMBER:
        if used_numbers.get(x, False) == False:
            ans.append(x)
            for i in range(x, MAX_NUMBER, x):
                used_numbers[i] = True
        x += 1
    return ans


def main():
    n = int(input())
    primes = create_n_primes(n)
    ans = 1
    warned = False
    for i in range(0, n):
        ans *= primes[i]
        if not warned and ans > CRITICAL_NUMBER:
            print(f"ответ больше {CRITICAL_NUMBER}")
            warned = True
    print(ans)


if __name__ == "__main__":
    main()
