#include <algorithm>
#include <iostream>

int main() {
    int max = -1e9, min = 1e9;
    int c;
    std::cin >> c;
    while (c != 0) {
        max = std::max(max, c);
        min = std::min(min, c);
        std::cin >> c;
    }
    std::cout << max - min << "\n";
}