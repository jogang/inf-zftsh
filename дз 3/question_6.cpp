#include <iostream>

int f(int x) {
    int l = 2 * x + 120;
    int m = x - 45;
    while (l != m) {
        if (l > m) {
            l = l - m;
        } else {
            m = m - l;
        }
    }
    return m;
}

int main() {
    int x = 401;
    while (f(x) != 30) {
        x++;
    }
    std::cout << x << "\n";
}