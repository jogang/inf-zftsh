#include <algorithm>
#include <iostream>
#include <vector>

using ll = long long;

// количество входных чисел >= 2
int main() {
    ll cur_ar_progression_size = 2, cur_geom_progression_size = 2;
    ll max_ar_progression_size = 2, max_geom_progression_size = 2;

    ll c, last, last2;
    std::cin >> last2 >> last >> c;
    while (c != 0) {
        // условие продолжения прогрессии
        if ((c - last) == (last - last2)) {
            cur_ar_progression_size++;
        } else {
            // из двух элементов (last и c) всегда можно составить
            // последовательность
            cur_ar_progression_size = 2;
        }

        if ((c * 1.0 / last) == (last * 1.0 / last2)) {
            cur_geom_progression_size++;
        } else {
            cur_geom_progression_size = 2;
        }

        // обновление максимума
        max_ar_progression_size =
            std::max(max_ar_progression_size, cur_ar_progression_size);
        max_geom_progression_size =
            std::max(max_geom_progression_size, cur_geom_progression_size);

        last2 = last;
        last = c;
        std::cin >> c;
    }
    std::cout << std::max(max_ar_progression_size, max_geom_progression_size)
              << "\n";
}