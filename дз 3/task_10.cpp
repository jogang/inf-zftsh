#include <array>
#include <iostream>
#include <string>

std::array<int, 10> digits(std::string s) {
    std::array<int, 10> ans;
    ans.fill(0);
    for (auto x : s) {
        ans[x - '0']++;
    }
    return ans;
}

bool able_to_construct(std::array<int, 10> a, std::array<int, 10> b) {
    bool ans = true;
    for (int i = 0; i <= 9; ++i) {
        if (a[i] < b[i]) ans = false;
    }
    return ans;
}

int main() {
    std::string a, b;
    std::cin >> a >> b;
    auto digits_a = digits(a), digits_b = digits(b);
    auto ans = able_to_construct(digits_a, digits_b) ||
               able_to_construct(digits_b, digits_a);
    std::cout << (ans ? "можно" : "нельзя") << "\n";
}